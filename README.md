# DIO: primeiro projeto Godot 2D

Este repositório deveria conter uma atualização aprimorada do jogo apresentado pelo instrutor Rafael Skoberg, sendo disponibilizada funcionalmente através da plataforma DIO.

No entanto, preferi adicionar os links onde a base do projeto original se encontra e criar meu próprio jogo, "do zero", em outro repositório. O jogo será publicado em minha conta na plataforma Itch.io, após finalizado (ou parcialmente jogável, pelo menos).

Deixo aqui minha recomendação pelos cursos da plataforma DIO, bem como pelos conteúdos do instrutor Rafael Skoberg.

**Atualiação:** Na branch "development", estou criando um jogo de tabuleiro. Ainda está nos estágios iniciais, com bugs conhecidos. Em breve, devo "trazer" para a branch principal uma versão jogável, mais estável.


### Referências

- [DIO - Digital Innovation One](https://dio.me) - "Plataforma gamificada de educação online com mais de 2.000 experiências educacionais em desenvolvimento de software entre cursos, desafios de codificação e projetos práticos." - _Retirado do Google_

- [DIO - Trilha Godot](https://github.com/digitalinnovationone/trilha-godot) - Repositório. O Bootcamp da DIO tratou apenas do projeto chamado "Tiny Swords". Os demais projetos contidos no repositório não foram estudados por mim (até o presente momento)

- "_Tiny Swords_" foi criado por Rafael Skoberg, utilizando um pacote de assets de mesmo nome. Este foi criado por [Pixel Frog](https://pixelfrog-assets.itch.io/tiny-swords).

- Além desta conta no Gitlab, também possuo uma no [Github](https://github.com/renatolipi), que pode ser visitada para fins de avaliação.

- Minha conta na plataforma Itch.io pode ser encontrada [aqui](https://renatolipi.itch.io/) e, até o presente momento, não possui nenhum projeto público.


.

_Atualizado em: 2024-06-03_



